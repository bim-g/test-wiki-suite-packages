#!/usr/bin/perl

package virtual_server;
if (!$module_name) {
	$main::no_acl_check++;
	$ENV{'WEBMIN_CONFIG'} ||= "/etc/webmin";
	$ENV{'WEBMIN_VAR'} ||= "/var/webmin";
	if ($0 =~ /^(.*)\/[^\/]+$/) {
		chdir($pwd = $1);
		}
	else {
		chop($pwd = `pwd`);
		}
	$0 = "$pwd/wikisuite-auto-wizard.pl";
	require './virtual-server-lib.pl';
	$< == 0 || die "auto-wizard.pl must be run as root";
	}
@OLDARGV = @ARGV;
&set_all_text_print();

if ( $config{'wizard_run'} == 1 ) {
    print "Wizard already executed, exiting ...\n";
    exit 0;
}

require './wizard-lib.pl';

my $step;
my @steps = &get_wizard_steps();

$step = "memory";
if ( grep( /^$step$/, @steps ) ) {
    my $memory;
    %$memory = (
        'preload' => 0,
    );
    $err = &wizard_parse_memory($memory);
    $err = 'No error' if (!$err);
    print "Memory: $err\n";
} else {
    print "Skipping $step Wizard\n";
}

$step = "db";
if ( grep( /^$step$/, @steps ) ) {
    my $db;
    %$db = (
        mysql => 1,
        postgres => 0,
    );
    $err = &wizard_parse_db($db);
    $err = 'No error' if (!$err);
    print "Db: $err\n";
} else {
    print "Skipping $step Wizard\n";
}

$step = "mysql";
if ( grep( /^$step$/, @steps ) ) {
    my $mysql;
    %$mysql = (
        mypass => &random_password(16),
    );
    $err = &wizard_parse_mysql($mysql);
    $err = 'No error' if (!$err);
    print "MySQL/MariaDB: $err\n";

} else {
    print "Skipping $step Wizard\n";
}

$step = "mysize";
if ( grep( /^$step$/, @steps ) ) {
    my $mysize;
    %$mysize = (
        mysize => 'huge',
    );
    $err = &wizard_parse_mysize($mysize);
    $err = 'No error' if (!$err);
    print "Mysize: $err\n";
} else {
    print "Skipping $step Wizard\n";
}

$step = "hashpass";
if ( grep( /^$step$/, @steps ) ) {
    my $hashpass;
    %$hashpass = (
        hashpass => 1,
    );
    $err = &wizard_parse_hashpass($hashpass);
    $err = 'No error' if (!$err);
    print "Hashpass: $err\n";
} else {
    print "Skipping $step Wizard\n";
}

# $step = "defdom";
# if ( grep( /^$step$/, @steps ) ) {
#     my $defdom;
#     %$defdom = (
#         'defdom' => true,
#         'defhost' => $ENV{'WIKISUITE_DEFHOST'},
#         'defssl' => 2,
#     );
#     $err = &setup_virtualmin_default_hostname_ssl($defdom);
#     $err = 'No error' if (!$err);
#     print "Defdom: $err\n";
# } else {
#     print "Skipping $step Wizard\n";
# }

$config{'wizard_run'} = 1;
&save_module_config();

print "done\n";
